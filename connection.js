const mysql = require('mysql2');

var mysqlConnection = mysql.createConnection({
    //host : '172.17.0.2',
    //port : 3306,
    host : 'localhost',
    port : 3305,
    user:'root',
    password:'root',
    database:'student_db',
})

mysqlConnection.connect((err)=>{
    if(err){
        console.log("Error in DB connection"+JSON.stringify(err,undefined,2));
    }else{
        console.log('DB connected successfully');
    }
})

module.exports = mysqlConnection;

