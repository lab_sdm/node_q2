const connection = require('./connection');

const express = require('express');
const bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());

app.get('/',(req,res)=>{
    res.send("Its Running");
})

app.get('/students',(req,res)=>{

    connection.query('SELECT * FROM stud',(err,rows)=>{
        if(err){
            res.send(err);
        }
        else{
            res.send(rows);
        }
    })
})


app.get('/students/:name',(req,res)=>{

    connection.query('SELECT * FROM stud where name = ?',[req.params.name],(err,rows)=>{
        if(err){
            res.send(err);
        }
        else{
            res.send(rows);
        }
    })
})

app.post('/',(req,res)=>{
    var body = req.body;
    connection.query('INSERT into stud values(?,?,?,?,?,?)',[req.body.id,req.body.name,req.body.course, req.body.pass_year, req.body.prn, req.body.dob ],(err,rows)=>{
        if(err){
            res.send(err);
        }
        else{
            res.send(rows);
        }
    })
})

app.put('/',(req,res)=>{
    var body = req.body;
    connection.query('UPDATE stud set course = ? and prn = ?',[body],(err,rows)=>{
        if(err){
            res.send(err);
        }
        else{
            res.send(rows);
        }
    })
})

app.delete('/:id',(req,res)=>{

    connection.query('DELETE FROM stud where id = ?',[req.params.id],(err,rows)=>{
        if(err){
            res.send(err);
        }
        else{
            res.send(rows);
        }
    })
})









app.listen('3000',()=>{
    console.log("server started successfully at port 3000");
})